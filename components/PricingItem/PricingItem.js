import Link from 'next/link'
import styles from './PricingItem.module.css'
const PricingItem = ({title,price,children}) => {
    return (
        <article className={`col-md-4 ${styles.cols}`}>
            <div className={styles.pricingItem}>
            <div className={styles.pricingHead}>
            <h3 className={styles.pricingItemTitle}>{title}</h3>
            <h5 className={styles.pricingItemPrice}>{price} <span className={styles.pricingItemSign}>$</span></h5>
            <Link href="/"><a className={styles.pricingItemBtn}>Order Now</a></Link>
                </div>
            <ul className={styles.pricingList}>
                {children}
            </ul>
            </div>
        </article>
    )
}

export default PricingItem
