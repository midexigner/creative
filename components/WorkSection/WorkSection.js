import Image from 'next/image'
import Heading from '../Heading/Heading'
import styles from './WorkSection.module.css'

const WorkSection = () => {
    return (
        <section id="work" className={styles.workSection}>
           <div className="container-fluid">
               <div className="row">
                 <Heading subtitle="WHAT WE HAVE DONE" title="OUR WORKS" line />
               </div>
               <div className="row row-cols-1 row-cols-md-2 row-cols-lg-3 g-0 gap-0">
               <figure>
               <div className={styles.overlay}></div>
                 <img src="/images/work/gallery-01.jpg" alt=""/>
               </figure>
               <figure>
               <div className={styles.overlay}></div>
               <img src="/images/work/gallery-02.jpg" alt=""/>
               </figure>
               <figure>
               <div className={styles.overlay}></div>
               <img src="/images/work/gallery-03.jpg" alt=""/>
               </figure>
               <figure>
               <div className={styles.overlay}></div>
               <img src="/images/work/gallery-04.jpg" alt=""/>
               </figure>
              <figure>
              <div className={styles.overlay}></div>
              <img src="/images/work/gallery-05.jpg" alt=""/>
              </figure>
               <figure>
                 <div className={styles.overlay}></div>
               <img src="/images/work/gallery-06.jpg" alt=""/>
               </figure>
               {/*  */}
               </div>
           </div>
        </section>
    )
}

export default WorkSection
