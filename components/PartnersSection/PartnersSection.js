import Image from 'next/image'
import styles from './PartnersSection.module.css'
const PartnersSection = () => {
    return (
        <section className={styles.PartnersSection}>
       <div className="container">
         <div className="row">
           <div className="col">
           <Image
        src="/images/partner/partner-01.png"
        alt="partner-01"
        width={276}
        height={151}
      />
           </div>
           <div className="col">
           <Image
        src="/images/partner/partner-02.png"
        alt="partner-02"
        width={276}
        height={151}
      />
           </div>
           <div className="col">
           <Image
        src="/images/partner/partner-03.png"
        alt="partner-03"
        width={276}
        height={151}
      />
           </div>
           <div className="col">
           <Image
        src="/images/partner/partner-04.png"
        alt="partner-04"
        width={276}
        height={151}
      />
           </div>
           <div className="col">
           <Image
        src="/images/partner/partner-05.png"
        alt="partner-05"
        width={276}
        height={151}
      />
           </div>
           
           
         </div>
       </div>
     </section>
    )
}

export default PartnersSection
