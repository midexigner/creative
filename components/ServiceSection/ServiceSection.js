import { useEffect } from 'react'
import styles from './ServiceSection.module.css'
import ServiceItem from '../ServiceItem/ServiceItem'
import Aos from 'aos'
import 'aos/dist/aos.css'

const ServiceSection = () => {
    useEffect(() => {
        Aos.init({ 
            easing: 'ease-in-sine',
            duration: 1000,
            // disable: window.innerWidth < 1024
        });
    }, [])
    return (
        <div id="services" className={styles.serviceSection}>
            <div className="container">
                <div className="row mb-5">
              <div className="col-md-4" data-aos="slide-left"> <h5> OUR SERVICES</h5></div>
              <div className="col-md-8"data-aos="slide-right">  <h2> We provide genius solutions for your business</h2></div>
              
                </div>
                <div className="row row-cols-1 row-cols-sm-3 row-cols-md-3 gap-auto  ">
                   <ServiceItem animation="fade-up" url="/images/icons-01.png" title="LOGO DESIGN" text="A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart" />
                   <ServiceItem animation="fade-up" url="/images/icons-02.png" title="STATIONERY DESIGN" text="A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart" />
                   <ServiceItem animation="fade-up" url="/images/icons-03.png" title="PRINT DESIGN" text="A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart" />
                   <ServiceItem  animation="fade-up"url="/images/icons-04.png" title="PRODUCT DESIGN" text="A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart" />
                   <ServiceItem animation="fade-up" url="/images/icons-05.png" title="WEB DESIGN" text="A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart" />
                   <ServiceItem animation="fade-up" url="/images/icons-06.png" title="CONTENT WRITING" text="A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart" />
                </div>
            </div>
        </div>
    )
}

export default ServiceSection
