import Image from 'next/image'
import Heading from '../Heading/Heading'
import styles from './TeamSection.module.css'

const TeamSection = () => {
    return (
        <section id="team" className={styles.teamSection}>
            <div className="container-fluid">
                <div className="row justify-content-center">
           <div className="col-md-5"> <Heading dark subtitle="OUR TEAM" text="We help company set up business strategy including design brand identity or product design service. So company can focus on their core business."  /></div>
                </div>
                <div className="row row-cols-1 row-cols-md-3 row-cols-lg-4 g-0 gap-0">
<figure>
<img src="/images/team/01.jpg" />
</figure>
<figure>
  <img src="/images/team/02.jpg" />
</figure>
<figure>
<img src="/images/team/03.jpg" />
</figure>
<figure>
<img src="/images/team/04.jpg" />
</figure>
                </div>
            </div>
        </section>
    )
}

export default TeamSection
