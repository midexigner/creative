import PricingItem from '../PricingItem/PricingItem'
import styles from './PriceSection.module.css'

const PriceSection = () => {
    return (
        <section id="price" className={styles.priceSection}>
            <div className="container">
            <div className="row">
                <PricingItem title="Basic" price="10">
                <li>2 LOGO CONCEPTS</li>
                <li>3  REVISIONS</li>
                <li> SOURCE FILES</li>
                </PricingItem>
                 <PricingItem title="Gold" price="30">
                 <li>4 LOGO CONCEPTS</li>
                <li>UNLIMITED REVISIONS</li>
                <li> SOURCE FILES</li>
                <li> SOCIAL MEDIA KIT</li>
                </PricingItem>
                <PricingItem title="Daimond" price="50">
                <li>6 LOGO CONCEPTS</li>
                <li>UNLIMITED REVISIONS</li>
                <li> SOURCE FILES</li>
                <li> SOCIAL MEDIA KIT</li>
                <li> STATIONERY DESIGN</li>
                    </PricingItem>
            </div>
            </div>
        </section>
    )
}

export default PriceSection
