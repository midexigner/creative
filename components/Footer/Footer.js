import React from 'react'
import Image from 'next/image'
import { FaFacebookF,FaWhatsapp,FaInstagram,FaLinkedinIn} from 'react-icons/fa';
import styles from './Footer.module.css'

export const Footer = () => {
    return (
        <footer className={styles.footer}>
<div className="container">
    <div className="row row-cols-1">
        <div className="col">
       <div className={styles.footerLogo}>
        <Image
        src="/images/footer-logo.png"
        alt="Footer Logo"
        width={429}
        height={71}
       
      />
      </div>
        </div>
    </div>
    <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-2">
        <aside>
            <p>Copyright All Right Reserved.</p>
        </aside>
        <aside>
        <ul className="list-unstyled">
            <li>0313-2667361</li>
            <li><a href="mailto:contact@creativeot.com">contact@creativeot.com</a></li>
            <li>Adress abc street defence ajao</li>
            </ul>
        </aside>
        <aside>
        <ul className="list-inline">
  <li className="list-inline-item"><a href="#" target="_blank" className={styles.social__link}><FaFacebookF /></a></li>
  <li className="list-inline-item"><a href="#" target="_blank" className={styles.social__link}><FaWhatsapp/></a></li>
  <li className="list-inline-item"><a href="#" target="_blank" className={styles.social__link}><FaInstagram/></a></li>
  <li className="list-inline-item"><a href="#" target="_blank" className={styles.social__link}><FaLinkedinIn/></a></li>
</ul>
        </aside>
    </div>
</div>
        </footer>
    )
}
