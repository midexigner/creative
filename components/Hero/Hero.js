import Link from 'next/link'
import styles from './Hero.module.css'
export const Hero = () => {
    return (
        <section id="hero" className={styles.hero}>
                   <div className={styles.heroSection}>
                   <h2>WE CREATE YOUR
 professional LOGO  </h2>
 <p>We Are A Skilled Creative Designers And We Help Your Business Success</p>
 <hr className="border-line"/>
 <Link href="/work"><a>View Our Work</a></Link>
                   </div>
        </section>
    )
}
