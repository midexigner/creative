import styles from './CountSection.module.css'
import CountUp from 'react-countup';

const CountSection = () => {
    return (
        <section className={styles.countSection}>
            <div className="contaniner">
                <div className="row justify-content-center gap-0 m-0">
                    <div className="col-md-6 text-center">
                        <h2>trusted by 2000+ clients</h2>
                        <div className="row">
                            <div className="col-md-6">
                           <div className={styles.counterSingle}>
                           <CountUp start={0} end={1419}  decimal="," duration={4}/>
                           <h5>Completed Projects</h5>
                           </div> 
                            </div>
                            <div className="col-md-6">
                                <div className={styles.counterSingle}>
                            <CountUp start={0} end={2981} decimal="," duration={4} />
                            <h5>Drink Tea</h5>
                            </div>
                           
                            </div>
                            <hr className={styles.borderVertical} />
                            <hr className="border-line" />
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default CountSection
