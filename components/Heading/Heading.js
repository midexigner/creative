import styles from './Heading.module.css'

const Heading = ({dark,subtitle,title,line,text}) => {
    return (
        <div className={`${styles.Heading} ${dark && styles.dark}`}>
           {subtitle &&  <h5>{subtitle}</h5>}
           {title &&  <h2>{title}</h2>}
            {line && <hr className="border-line"/>}
            {text && <p>{text}</p>}
        </div>
    )
}

export default Heading
