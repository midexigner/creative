import Image from 'next/image'
import styles from './ServiceItem.module.css'

const ServiceItem = ({animation,url,title,text}) => {
    return (
        <article data-aos={animation} className={styles.ServiceItem}>
            <figure>
            <Image
        src={url}
        alt={title}
        width={51}
        height={47}
      />
            </figure>
            {title &&  <h3>{title}</h3>}
            {title &&  <p>{text}</p>}
            <hr className="border-line2"/>
        </article>
    )
}

export default ServiceItem
