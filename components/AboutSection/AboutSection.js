import { useEffect } from 'react'
import Aos from 'aos'
import 'aos/dist/aos.css'
import Link from 'next/link'
import styles from './AboutSection.module.css'
import {BsArrowRight} from 'react-icons/bs'

const AboutSection = () => {

    useEffect(() => {
        Aos.init({ 
            easing: 'ease-in-sine',
            duration: 1000,
            // disable: window.innerWidth < 1024
        });
    }, [])

    return (
        <section className={styles.aboutSection}>
          <div className="container">
              <div className="row">
                  <div className="col-md-7" data-aos="slide-right">
                      <h5>About Us</h5>
                      <h2>We are a super awesome creative agency.</h2>
                  </div>
                  <div className="col-md-5" data-aos="slide-left">
                      <p>We help company set up business strategy
 including design brand identiy or product design service. So company can focus on their core 
business. A wonderful serenity has taken
 possession of my entire soul, like these sweet.</p>
 <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in 
Bookmarksgrove right at the coast of the 
Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
<Link href="/"><a>MORE ABOUT US <BsArrowRight/></a></Link>
                  </div>
              </div>
          </div>
        </section>
    )
}

export default AboutSection
