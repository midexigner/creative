import React, { useEffect, useState } from 'react'
import Image from 'next/image'
import {Link} from 'react-scroll'
import styles from './Header.module.css'

const Header = () => {
  const [show, handleShow] = useState(false)
  useEffect(()=>{
      window.addEventListener("scroll",()=>{
          if(window.scrollY > 100){
              handleShow(true);
          } else handleShow(false);
      });
      return ()=>{
         window.removeEventListener("scroll") 
      }
  },[])

    return (
        <nav className={`navbar navbar-expand-lg navbar-light ${styles.header} ${show && styles.nav__black}`}>
        <div className="container">
          <Link className="navbar-brand" to="hero">
       
        <Image
        src="/images/logo.png"
        alt="Logo"
        width={350}
        height={59}
      />
      </Link>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
              <li className={styles.navItem}>
                
                <Link activeClass="active" className={styles.navLink} to="hero" spy={true} smooth={true}>Home</Link>
              </li>
              <li className={styles.navItem}>
                <Link activeClass="active" className={styles.navLink} to="price" spy={true} smooth={true}>Price</Link>
              </li>
              <li className={styles.navItem}>
                <Link activeClass="active" className={styles.navLink} to="work" spy={true} smooth={true}>Work</Link>
              </li>
              <li className={styles.navItem}>
                <Link activeClass="active" className={styles.navLink} to="services" spy={true} smooth={true}>Services</Link>
              </li>
              <li className={styles.navItem}>
              <Link activeClass="active" className={styles.navLink} to="team" spy={true} smooth={true}>Team</Link>
              </li>
              <li className={styles.navItem}>
                <a className={styles.navLink} href="/">Contact</a>
              </li>
            
              
            </ul>
            
          </div>
        </div>
      </nav>
    )
}

export default Header
