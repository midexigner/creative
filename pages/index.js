import Head from "next/head";
import AboutSection from "../components/AboutSection/AboutSection";
import CountSection from "../components/CountSection/CountSection";
import { Hero } from "../components/Hero/Hero";
import PartnersSection from "../components/PartnersSection/PartnersSection";
import PriceSection from "../components/PriceSection/PriceSection";
import ServiceSection from "../components/ServiceSection/ServiceSection";
import TeamSection from "../components/TeamSection/TeamSection";
import WorkSection from "../components/WorkSection/WorkSection";

export default function Home() {
  return (
    <div className={`home`}>
      <Head>
        <title>Creative OT</title>
      </Head>
      <Hero />
      <PriceSection />
      <WorkSection />
      <AboutSection />
      <ServiceSection />
      <TeamSection/>
      <CountSection />
      <PartnersSection />
    </div>
  );
}
