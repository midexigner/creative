import { Footer } from '../components/Footer/Footer.js'
import Header from '../components/Header/Header.js'
import globalStyles from '../styles/globals.js'

function MyApp({ Component, pageProps }) {
  return (
  <>
        <Header/>
        <Component {...pageProps} />
        <style jsx global>
        {globalStyles}
      </style>
      <Footer />
  </>
  
  )
}

export default MyApp
